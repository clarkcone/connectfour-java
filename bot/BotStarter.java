// // Copyright 2015 theaigames.com (developers@theaigames.com)

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.
//	
//    For the full copyright and license information, please view the LICENSE
//    file that was distributed with this source code.

package bot;

public class BotStarter {	     

     //returns column where the turn was made
     public int makeTurn(int round, Field field) {
    	 //if we move first, put our piece in the proven, winning pos
    	 //compete for that middle spot
    	 if(evaluate(round, field) <= 0 && !field.isColumnFull(3))
    		 return 3;
    	 return evaluate(round, field);
     }
     
     public int evaluate(int round, Field field){
    	 //place the piece in a column that corresponds to the board with the highest utility
    	 int fin = findLargest(assessFutureBoards(lookAhead(field, 1)));
    	 //be sure that our bot doesnt do something stupid and we're not placing it in a full column
    	 while (field.isColumnFull(fin)){
    		 fin = fin + 1;
    	 }
    	 return fin;
     }
     	
 	//look ahead at all possible boards
 	//generate an array of boards, each with having placed a different piece in each position
 	public Field[] lookAhead(Field field, int disc){
		Field[] future = new Field[7];
		for(int i = 0; i < 7; i++){
			future[i] = generateNew(field, i, disc);
		}
		return future;
 	}
 	
 	//generate new board
 	public Field generateNew(Field field, int col, int disc){
 		//copy the current board into a new board
 		Field futureField = field;
 		//add a disc to it
 			//could be the source of bugs?
 		futureField.addDisc(col, disc);
 		return futureField;
 	}
 	
 	//returns an array of utilities corresponding to the 7 different boards after each move
 	public int[] assessFutureBoards(Field[] future){
 		int[] utility = new int[7];
 		for(int i = 0; i < 7; i++){
 			utility[i] += detectFullColumns(future[i]);
 			utility[i] += detectTwoHorizontally(2, future[i]);
 			utility[i] += detectTwoVertically(2, future[i]);
 			utility[i] += detectThreeHorizontally(2, future[i]);
 			utility[i] += detectThreeVertically(2, future[i]);
 		}
 		return utility;
 	}
 	
 	//give a huge negative reward for full columns
 	//should help ensure that our bot does not place a piece there
 	public int detectFullColumns(Field field){
 		int util = 0;
 		for(int i = 0; i < 7; i++){
 			if(field.isColumnFull(i)){
 				util = -100000000;
 			} else {
 				util = 0;
 			}
 		}
		return util;
 	}
 	
 	//detects if there are two in a row horizontally
 	public int detectTwoHorizontally(int enemyID, Field field){
		int worth = 0;
		int[][] mBoard = field.getmBoard();
		for (int j = 0; j < 6; j++){
			for (int i = 0; i < 4; i++){
				//2200
				if(		mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == 0 &&
						mBoard[i+3][j] == 0	)
					worth -= 2500;
				//0022
				else if(mBoard[i][j] == 0 &&
						mBoard[i+1][j] == 0 &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == enemyID	)
					worth -= 2500;
				//2020
				else if(mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == 0 &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == 0	)
					worth -= 2500;
				//0202
				else if(mBoard[i][j] == 0 &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == 0 &&
						mBoard[i+3][j] == enemyID	)
					worth -= 2500;
				//0220
				else if(mBoard[i][j] == 0 &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == 0	)
					worth -= 2500;
				//2002
				else if(mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == 0 &&
						mBoard[i+2][j] == 0 &&
						mBoard[i+3][j] == enemyID	)
					worth -= 2500;
			}
		}
		return worth;
 	}
 	
 	//detects if there are two in a row vertically
 	public int detectTwoVertically(int enemyID, Field field){
		int worth = 0;
		int[][] mBoard = field.getmBoard();
		for(int i = 5; i > 1; i--){
			for (int j = 0; j < 7; j++){
				/*
				 * 0
				 * 2
				 * 2
				 */
				if(		mBoard[j][i] == enemyID &&
						mBoard[j][i-1] == enemyID &&
						mBoard[j][i-2] == 0
												)
					worth -= 5000;
			}
		}
		return worth;
 	}
 	
	//detects if we have 3 in a row on a board and returns a utility value of the board
	public int detectThreeHorizontally(int enemyID, Field field){
		int worth = 0;
		int[][] mBoard = field.getmBoard();
		for (int j = 0; j < 6; j++){
			for (int i = 0; i < 4; i++){
				//2202
				if(		mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == 0 &&
						mBoard[i+3][j] == enemyID	)
					worth -= 20000;
				//2022
				else if(mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == 0 &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == enemyID	)
					worth -= 20000;
				//0222
				else if(mBoard[i][j] == 0 &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == enemyID	)
					worth -= 20000;
				//2220
				else if(mBoard[i][j] == enemyID &&
						mBoard[i+1][j] == enemyID &&
						mBoard[i+2][j] == enemyID &&
						mBoard[i+3][j] == 0	)
					worth -= 20000;
			}
		}
		return worth;
	}
	
	//detects if we have 3 in a column on the board
	/*
	 * 0
	 * 2
	 * 2
	 * 2
	 */
	public int detectThreeVertically(int enemyID, Field field){
		int worth = 0;
		int[][] mBoard = field.getmBoard();
		//only need to scan between the second and 5th rows
		//impossible to happen elsewhere
		for (int j = 5; j > 2; j--){
			for (int i = 0; i < 7; i++){
				if(		mBoard[i][j] == enemyID &&
						mBoard[i][j-1] == enemyID &&
						mBoard[i][j-2] == enemyID &&
						mBoard[i][j-3] == 0)
					worth -= 20000;
			}
		}
		return worth;
	}
        
     //helper to find the largest value in an array
     public static int findLargest(int[] rewards){
         int largest = rewards[0];
         int largestIndex = 0;

         for(int i = 0; i < rewards.length; i++){
             if(rewards[i] > largest) {
                 largest = rewards[i]; 
                 largestIndex = i;
             }  
         }
         return largestIndex;
     }
     
 	public static void main(String[] args) {
 		BotParser parser = new BotParser(new BotStarter());
 		parser.run();
 	}
 	
 }
